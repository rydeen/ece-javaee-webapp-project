# Enterprise Middleware with Enterprise Java #

ECE Paris course - fall 2015

Final project.


## Setup ##
1. import project to eclipse
2. create a new server (tomcat 8 was the one used during the development)
3. start and add the project to the server created

## Login ##
There are only two accounts created.

|	username	|	password	|
|----------|----------|
|	luis	|	123	|
|	rossana	|	123	|


## URLs ##

* localhost:port/WebApp/index.jsp - initial page
* localhost:port/WebApp/error.jsp - error page displayed when the authentication fails. This page cannot be accessed directly with this url.
* localhost:port/WebApp/demandeConge.jsp - user page online available after authenticated successfully. In this page the user can select one number to represent the day to request a leave.
* localhost:port/WebApp/resultatDemand.jsp - after the user submits a leave request, he will be foward to this page where he can se the result: Success / Failure. This page cannot be accessed directly with this URL.

## Redirects ##
* error.jsp to index.jsp if not authenticated
* error.jsp to demandeConge.jsp if already authenticated
* index.jsp to demandeConge.jsp if already authenticated
* demandeConge.jsp to index.jsp if not authenticated
* resultatDemand.jsp to index.jsp if not authenticated

## Cookies and Session ##
For the first time, the user will ask the server for authentication.
After being successful, the server will send 2 cookies: one with the value of the username and the other with a hash key generate by the servlet **authentication.AuthentificationServlet.java**.

These cookies will last for 4 days. If the user tries to authenticate again and still have these cookies, the servlet will only check if the username is the same as in one of the cookie and if the key inside the second cookie is correct. If so, the user will be authenticated with success.

If the user is inactive for 50 seconds or more, the session will expire and the user will have to ask for authentication again. Please note that, when the session expires, the cookies are not destroyed.

In case of a new user, previous cookies will be delected and new ones will be created and sent to the browser by the servlet.

## Screenshots of the different pages ##
### index.jsp ###
![Screen Shot 2015-11-26 at 1.56.11 AM.png](https://bitbucket.org/repo/q4X9Gz/images/908566589-Screen%20Shot%202015-11-26%20at%201.56.11%20AM.png)

### error.jsp ###
![Screen Shot 2015-11-26 at 1.56.25 AM.png](https://bitbucket.org/repo/q4X9Gz/images/3373318597-Screen%20Shot%202015-11-26%20at%201.56.25%20AM.png)

### demandeConge.jsp ###
![Screen Shot 2015-11-26 at 1.56.35 AM.png](https://bitbucket.org/repo/q4X9Gz/images/3549147114-Screen%20Shot%202015-11-26%20at%201.56.35%20AM.png)

### resultatDemade.jsp (Success) ###
![Screen Shot 2015-11-26 at 1.56.54 AM.png](https://bitbucket.org/repo/q4X9Gz/images/3533270316-Screen%20Shot%202015-11-26%20at%201.56.54%20AM.png)

### resultatDemade.jsp (Failure) ###
![Screen Shot 2015-11-26 at 1.56.45 AM.png](https://bitbucket.org/repo/q4X9Gz/images/3670228938-Screen%20Shot%202015-11-26%20at%201.56.45%20AM.png)